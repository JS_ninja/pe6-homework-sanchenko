const { series, parallel, watch, src, dest } = require("gulp");
const browserSync = require("browser-sync").create();

// gulp.series(), gulp.src(), gulp.dest()

// gulp.task("css", function (cb) {
//     console.log("css done");
//     cb();
// });

// gulp.task("default", gulp.series("css"));

const serv = () => {
    browserSync.init({
        server: {
            baseDir: "./",
        },
    });
};

const bsReload = (cb) => {
    browserSync.reload();
    cb();
};

const css = () => {
    return src("./src/css/style.css")
        .pipe(dest("./dist/styles/"))
        .pipe(browserSync.stream());
};
const js = () => {
    return src("./src/js/index.js")
        .pipe(dest("./dist/js/"))
        .pipe(browserSync.stream());
};

const watcher = (cb) => {
    watch("./index.html", bsReload);
    watch("./src/css/style.css", css);
    watch("./src/js/index.js", js);
    cb();
};

exports.default = parallel(serv, watcher, series(css, js)); //series(css, js, images)
exports.css = css;
// export default defaultTask;
