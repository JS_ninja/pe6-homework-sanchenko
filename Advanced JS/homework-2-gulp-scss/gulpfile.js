const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const browserSync = require("browser-sync").create();
const imagemin = require("gulp-imagemin");
const clean = require("gulp-clean");
const autoprefixer = require("gulp-autoprefixer");
const cleanCss = require("gulp-clean-css");
const uglify = require("gulp-uglify");
const jsMinify = require("gulp-js-minify");
const concat = require("gulp-concat");

function goImg() {
    return gulp.src("./src/img/*")
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.mozjpeg({quality: 75, progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest("./dist/img/"))
}

function goSass() {
    return gulp.src("./src/sass/**/*.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 40 versions'],
            cascade: false
        }))
        .pipe(cleanCss())
        .pipe(gulp.dest("./dist/css/"))
}

function goJS() {
    return gulp.src("./src/js/*")
        .pipe(uglify())
        .pipe(jsMinify())
        .pipe(gulp.dest("./dist/js"))
}

function reload(cb) {
    browserSync.reload();
    cb();
}

function goClean() {
    return gulp.src('./dist', { read: false, allowEmpty: true })
        .pipe(clean());
}

function dev(cb) {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch("./src/sass/**/*.scss", {usePolling: true}, gulp.series(goSass, reload));
    gulp.watch("./src/js/**/*.js", {usePolling: true}, gulp.series(goJS, reload));
    cb();
}

exports.dev = dev;
exports.goClean = goClean;

exports.build = gulp.series(goClean, goImg, goSass, goJS);



