const burgerButton = document.querySelector("div.header__burger-button");
const headerMenu = document.querySelector("nav.header__menu");
let burgerFold = true;

function clickHandler(e) {
    if (e.target === burgerButton) {
        burgerFold = !burgerFold;
        burgerAction(burgerFold);
    } else if (!burgerFold && (e.target.closest("nav.header__menu") !== headerMenu)) {
        burgerFold = !burgerFold;
        burgerAction(burgerFold);
    }

}

function burgerAction(burgerFold) {
    if (burgerFold) {
        for (let i = 0; i < burgerButton.children.length; i++) {
            burgerButton.children[i].classList.remove("burger-button__part--unfold");
            burgerButton.children[i].classList.add("burger-button__part--fold");
            headerMenu.classList.remove("header__menu--unfold")
        }
    } else {
        for (let i = 0; i < burgerButton.children.length; i++) {
            burgerButton.children[i].classList.remove("burger-button__part--fold");
            burgerButton.children[i].classList.add("burger-button__part--unfold");
            headerMenu.classList.add("header__menu--unfold")
        }
    }
}

document.addEventListener("click", clickHandler);