// const body = document.body;
// const list = document.getElementById("list");
// const listItems = [...document.querySelectorAll(".list-item")];
//
// function itemClickHandler(e) {
//     console.log("element click", e.target);
// }
//
// listItems.forEach((el) => {
//     el.addEventListener("click", itemClickHandler);
// });
//
// list.addEventListener("click", function itemClickHandler(e) {
//     if (e.target.classList.contains("list-item")) {
//         console.log("list click", e.target);
//     }
// });
//
// body.addEventListener("click", function itemClickHandler(e) {
//     console.log("body click", e.target);
// });
//
// list.addEventListener(
//     "click",
//     function itemClickHandler(e) {
//         if (e.target.classList.contains("list-item")) {
//             console.log("list click", e.target);
//         }
//     },
//     true
// );
//
// body.addEventListener(
//     "click",
//     function itemClickHandler(e) {
//         // e.stopPropagation();
//         console.log("body click", e.target);
//     },
//     true
// );
//
// document.querySelector("a").addEventListener("click", function (e) {
//     e.preventDefault();
//     console.log(e.defaultPrevented);
// });
//
// // create custom events
//
// const catFound = new CustomEvent("animalfound", {
//     detail: {
//         name: "cat",
//     },
// });
// const dogFound = new CustomEvent("animalfound", {
//     detail: {
//         name: "dog",
//     },
// });
//
// console.log(catFound);
// console.log(dogFound);
//
// // add an appropriate event listener
// document.addEventListener("animalfound", (e) => console.log(e.detail.name));
//
// // dispatch the events
// document.dispatchEvent(catFound);
// document.dispatchEvent(dogFound);

// const list = document.querySelector("#menu");
// let lastActive;
// list.addEventListener("click", function (e) {
//     e.preventDefault();
//     (lastActive)?lastActive.classList.remove("active"):true;
//     e.target.classList.add("active");
//     lastActive = e.target
// });

const animals = [...document.querySelectorAll(".pane")];

animals.forEach(function (el){
    el.children[2].addEventListener("click", function (event){
        this.parentElement.style.display = "none";
    })
});