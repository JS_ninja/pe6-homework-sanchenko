"use strict";

// const p = document.querySelector("p");
// const input = document.querySelector("input");
// const button = document.querySelector("button");

// p.addEventListener("click", function (e) {
//     console.log(e);
// });
// input.addEventListener("input", function (e) {
//     console.log(e);
// });
// button.addEventListener("keypress", function (e) {
//     console.log(e);
// });

// console.log(window.getComputedStyle(p).width);

// console.log("offsetHeight => ", p.offsetHeight);
// console.log("offsetWidth => ", p.offsetWidth);
// console.log("offsetTop => ", p.offsetTop);
// console.log("offsetLeft => ", p.offsetLeft);

// console.log("clientHeight => ", p.clientHeight);
// console.log("clientWidth => ", p.clientWidth);
// console.log("clientTop => ", p.clientTop);
// console.log("clientLeft => ", p.clientLeft);

// console.log("scrollHeight => ", p.scrollHeight);

// console.log("Body scrollHeight => ", document.body.scrollHeight);

// console.log("innerHeight => ", window.innerHeight);
// console.log("innerWidth => ", window.innerWidth);
// console.log("scrollX => ", window.scrollX);
// console.log("scrollY => ", window.scrollY);

// console.log("body clientHeight => ", document.body.clientHeight);
// console.log("body clientWidth => ", document.body.clientWidth);
// console.log("body clientTop => ", document.body.clientTop);
// console.log("body clientLeft => ", document.body.clientLeft);

// console.log(p.getBoundingClientRect());
// console.log(input.getBoundingClientRect());
// console.log(document.body.getBoundingClientRect());

// window.addEventListener("scroll", function (e) {
//     console.log("scroll");
// });

// TASK 2

// const input = document.querySelector("input");
// const p = document.createElement("p");
//
// input.after(p);
//
// document.addEventListener("keydown", (e)=>{p.textContent = "" + input.value});
// document.addEventListener("keyup", (e)=>{p.textContent = "" + input.value});

// TASK 3

// const p = document.querySelector("p");
//
// let checker = false;
//
// function handler(e) {
//     let a = p.getBoundingClientRect().top;
//     let b = p.getBoundingClientRect().top + p.getBoundingClientRect().height;
//     let c = window.innerHeight;
//
//     if (a > 0 && b < c && !checker) {
//         alert("Теперь вам все видно!");
//         checker = true;
//     }
//     if (checker && (a < 0 || b > c)) {
//         checker = false;
//     }
// }
//
// document.addEventListener("scroll", handler);

// TASK 4

// const text = document.querySelector("textarea");
//
// text.addEventListener("keydown", (e)=>{
//     if(e.keyCode >= 48 && e.keyCode <= 57){
//         console.log(`nope ${e.key} is not allowed`);
//         e.preventDefault();
//     }
// });

// TASK 5

// const input = document.querySelector("input");
// input.addEventListener("keydown", (e)=>{
//     if(e.key !== e.key.toLowerCase() && e.key === e.key.toUpperCase() ){
//         e.preventDefault();
//         console.log(`Это ж большая буква - ${e.key}`)
//     }
// });

// TASK 6

