const banners = [...document.querySelectorAll(".image-to-show")];
let bannerCounter = banners.indexOf(document.querySelector(".active"));
const timeLeft = document.querySelector("#timeleft");
const DELAY = 3000;
let timer = DELAY;
let mainInterval;
let isStop = false;

function changeBanner() {

    banners[bannerCounter].classList.add("deactivate");
    let changeBannerTimeout = setTimeout(() => {

        banners[bannerCounter].classList.remove("active");
        banners[bannerCounter].classList.remove("deactivate");
        (bannerCounter === banners.length - 1) ? bannerCounter = 0 : ++bannerCounter;

        banners[bannerCounter].classList.add("active");
        banners[bannerCounter].classList.add("activate");

        let changeBannerTimeoutOneMore = setTimeout(() => {
            banners[bannerCounter].classList.remove("activate");

            clearTimeout(changeBannerTimeoutOneMore);
        }, DELAY/6);


        clearTimeout(changeBannerTimeout);
    }, DELAY/6);


}

function resetTimer() {
    timer = DELAY;
}

function timerCounter(timer) {
    let countStart = Date.now();
    let timerStart = timer;
    let timerInterval = setInterval(() => {
        timer = timerStart - (Date.now() - countStart);
        let seconds = Math.floor(timer / 1000);
        let milliseconds = timer - seconds * 1000;
        timeLeft.textContent = `${seconds} s, ${milliseconds} ms`;
        if (timer <= 1) {
            clearInterval(timerInterval);
            timeLeft.textContent = `0 s, 0 ms`;
            resetTimer();
        }
        if (isStop) {
            clearInterval(timerInterval);
            isStop = false;
        }
    }, 10);

}

function mainIntervalHandler() {
    changeBanner();
    timerCounter(timer);
}

function go() {
    console.log("Here we go!");
    mainInterval = setInterval(mainIntervalHandler, DELAY);
    timerCounter(timer);
}

function stop() {
    console.log("Here we stop!");
    isStop = true;
    clearInterval(mainInterval);
}

go();

const buttonHandler = function () {
    if (button.textContent === "Прекратить") {
        stop();
        button.textContent = "Возобновить показ";
    } else {
        go();
        button.textContent = "Прекратить";
    }
};
const button = document.querySelector("#button");
button.addEventListener("click", buttonHandler);
window.addEventListener("keydown", (e) => {
    if (e.code === "Space") {
        buttonHandler();
    }
});