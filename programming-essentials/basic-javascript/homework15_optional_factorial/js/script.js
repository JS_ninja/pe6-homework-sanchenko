let x = "";

function getFactorial(num1, num2) {
    if (num1 === 0) {
        return 1;
    } else if (num1 === 1) {
        return num2;
    } else {
        num1 = num1 - 1;
        num2 = num2 * num1;
        getFactorial(num1, num2);
    }
}

do {
    x = prompt("Write a number", x);
} while (!isNumber(x) || Number(x) < 0);

x = Number(x);

alert(`Factorial of your number is ${getFactorial(x, x)}`);
