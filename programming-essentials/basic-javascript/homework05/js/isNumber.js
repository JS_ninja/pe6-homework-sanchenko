function isNumber(x) {
    return !(!(x && !isNaN(+x) && typeof +x === "number"));
}