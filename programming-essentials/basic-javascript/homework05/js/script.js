const today = new Date();

function isDateCorrect(x) {
    if (x.length !== 10) {
        return false;
    }

    let dd = +(x[0] + x[1]);
    let mm = +(x[3] + x[4]);
    let yyyy = +(x[6] + x[7] + x[8] + x[9]);

    return !((yyyy < 1900 || yyyy > today.getFullYear()) || (mm < 1 || mm > 12) || (dd < 1 || ((mm === 1 || mm === 3 || mm === 5 || mm === 7 || mm === 8 || mm === 10 || mm === 12) && dd > 31) || ((mm === 4 || mm === 6 || mm === 9 || mm === 11) && dd > 30) || (mm === 2 && ((yyyy % 4 === 0 && dd > 29) || yyyy % 4 !== 0 && dd > 28))));
}

function createNewUser() {
    let previousA = "";
    let a = "";
    let previousB = "";
    let b = "";
    let previousC = "";
    let c = "";

    do {
        a = prompt("Write your first name", previousA);
        if (a !== null) {
            previousA = a;
        }
    }
    while (isNumber(a) || a === null);

    do {
        b = prompt("Write your last name", previousB);
        if (b !== null) {
            previousB = b;
        }
    }
    while (isNumber(b) || b === null);

    do {
        c = prompt("Write your birthday date (dd.mm.yyyy)", previousC);
        if (c !== null) {
            previousC = c;
        }
    }
    while (!isDateCorrect(c));

    let d = new Date(+(c[6] + c[7] + c[8] + c[9]), +(c[3] + c[4]) - 1, +(c[0] + c[1]));

    return new Object({
        _firstName: a,
        set firstName(value) {
            let agreed = confirm("To change first name, write it in the next dialog window");
            if (agreed) {
                do {
                    this._firstName = prompt("Here you go:");
                    if (this._firstName !== null) {
                        previousA = this._firstName;
                    }
                } while (isNumber(this._firstName) || this._firstName === null)
            }
        },
        get firstName() {
            return this._firstName
        },
        _lastName: b,
        set lastName(value) {
            let agreed = confirm("To change last name, write it in the next dialog window");
            if (agreed) {
                do {
                    this._lastName = prompt("Here you go:");
                    if (this._lastName !== null) {
                        previousB = this._lastName;
                    }
                } while (isNumber(this._lastName) || this._lastName === null)
            }
        },
        get lastName() {
            return this._lastName
        },
        _birthDate: d,
        set birthDate(value) {
            alert("Sorry, you're already born. You can't change this.");
        },
        get birthDate() {
            return this._birthDate;
        },
        getLogin: function () {
            return a[0].toLowerCase() + b.toLowerCase();
        },
        getAge: function () {
            return (this._birthDate.getMonth() < today.getMonth())?(today.getFullYear()-this._birthDate.getFullYear()):(this._birthDate.getMonth() > today.getMonth())?(today.getFullYear()-this._birthDate.getFullYear()-1):(this._birthDate.getDate() > today.getDate())?(today.getFullYear()-this._birthDate.getFullYear()-1):(today.getFullYear()-this._birthDate.getFullYear());
        },
        getPassword: function() {
            return (this._firstName[0].toUpperCase()+this._lastName.toLowerCase()+this._birthDate.getFullYear())
        }

    })
}


let newUser = createNewUser();

console.log("Login:");
console.log(newUser.getLogin());
console.log("Age:");
console.log(newUser.getAge());
console.log("Password:");
console.log(newUser.getPassword());
