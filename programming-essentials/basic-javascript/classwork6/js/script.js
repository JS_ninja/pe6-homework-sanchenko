const user = {
        name: "Ivan",
        surname: "Petrenko",
        job: "cleaner",

        sayHi: function () {
            console.log(`Hello! I am ${user.name} ${user.surname}`)
        },

        changeProperty: function (propertyName, propertyValue) {
            if ([propertyName] in user) {
                this[propertyName] = propertyValue;
            } else {
                console.log("Error!")
            }
        },
        createProperty: function (propertyName, propertyValue) {
            if (!([propertyName] in user)) {
                this[propertyName] = propertyValue;
            } else {
                console.log("Error!")
            }
        },
        allProperties: function () {
            for (let key in this){
                console.log()
            }
        }
    }
;

user.sayHi();
user.changeProperty("name", "Vasya");
user.createProperty("hobby", "watching TV");
user.sayHi();
user.allProperties();

