const switcher = document.querySelector(".dark_theme_switcher");
let isDark = false;
if (localStorage.getItem("dark") !== null) {
    isDark = JSON.parse(localStorage.getItem("dark"));
}

const wrapper = document.querySelector(".wrapper");
const pageMain = document.querySelector(".page-main");
const textInputs = [...document.querySelectorAll(".text-input")];


function switchColorScheme() {
    isDark = !isDark;
    localStorage.setItem("dark", JSON.stringify(isDark));
    checkColorScheme();
}

function checkColorScheme() {
    if (isDark) {
        wrapper.classList.add("wrapper_dark");
        pageMain.classList.add("page-main_dark");
        textInputs.forEach((el)=>{el.classList.add("text-input_dark")});
    } else {
        wrapper.classList.remove("wrapper_dark");
        pageMain.classList.remove("page-main_dark");
        textInputs.forEach((el) => {
            el.classList.remove("text-input_dark")
        });

    }
}

switcher.addEventListener("click", switchColorScheme);

checkColorScheme();