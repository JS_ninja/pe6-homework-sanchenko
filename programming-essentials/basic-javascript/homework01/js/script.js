let name = "";
let age = "";
let agreement;

function isItANumber(x) {
    return !(!(x && !isNaN(+x) && typeof +x === "number"));
}

do {
    do {
        name = prompt("What's your name?", name);
    } while (!name || isItANumber(name));
    age = prompt("How old are you?", age);
} while (!age || !isItANumber(age));

if (age < 18) {
    alert("You are not allowed to visit this website");
} else if (18 <= age && age <= 22) {
    agreement = confirm("Are you sure you want to continue?");
    if (agreement === true) {
        alert("Welcome, " + name);
    } else {
        alert("You are not allowed to visit this website.")
    }
} else {
    alert("Welcome, " + name);
}