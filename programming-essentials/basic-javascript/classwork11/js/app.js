"use strict";

const LIGHT_CELL = '#ffffff';
const DARK_CELL = '#161619';
const V_CELLS = 8;
const H_CELLS = 8;
const board = document.createElement('section');
board.classList.add('board');
board.style.cssText = `border:1px solid black;display: inline-block; background-color:${LIGHT_CELL}`;
const fragment = document.createDocumentFragment();
document.body.style.cssText = "display: flex;justify-content:center;";

const fillChessBoard = function () {
    fragment.append(board);

    for (let i = 0; i < V_CELLS; i++) {
        let vLine = document.createElement("div");
        vLine.cusomCounter = i+1;
        vLine.style.cssText = "height: 30px;";

        for (let i = 0; i < H_CELLS; i++) {
            let hCell = document.createElement("div");
            hCell.className = "cell";

            if (vLine.cusomCounter % 2 === 0) {
                if (i % 2 === 0) {
                    hCell.style.cssText = `display:inline-block; height: 30px; width: 30px;background-color:${LIGHT_CELL};`;
                } else {
                    hCell.style.cssText = `display:inline-block; height: 30px; width: 30px;background-color:${DARK_CELL};`;
                }
            } else {
                if (i % 2 === 0) {
                    hCell.style.cssText = `display:inline-block; height: 30px; width: 30px;background-color:${DARK_CELL};`;
                } else {
                    hCell.style.cssText = `display:inline-block; height: 30px; width: 30px;background-color:${LIGHT_CELL};`;
                }
            }


            vLine.innerHTML += hCell.outerHTML;
        }

        board.append(vLine);
    }

    document.body.prepend(fragment);
};

// const fillChessBoard = function () {
//     fragment.append(board);
//
//     for (let i = 1; i <= V_CELLS; i++) {
//         let vLine = document.createElement("div");
//         vLine.cusomCounter = i;
//         vLine.style.cssText = "height: 30px;";
//
//         for (let i = 1; i <= H_CELLS; i++) {
//             let hCell = document.createElement("div");
//             hCell.className = "cell";
//             hCell.style.cssText = `display:inline-block; height: 30px; width: 30px;`;
//
//             if (vLine.cusomCounter % 2 === 0) { // чётные строки
//                 if (vLine.cusomCounter === 1 || vLine.cusomCounter === V_CELLS) { // первая/последняя строка
//                     if (i % 2 === 0) {   // чётные ячейки в чётных строках
//                         hCell.style.cssText += `background-color:${DARK_CELL}`; // делаем чёрной
//                     }
//                 } else { // не первая/последняя
//                     if (i % 2 === 0 && (i === 1 || i === H_CELLS)) {   // чётные ячейки в чётных строках, но только первая/последняя ячейка в строке
//                         hCell.style.cssText += `background-color:${DARK_CELL}`; // делаем чёрной
//                     }
//                 }
//
//             } else { // нечётные строки
//                 if (vLine.cusomCounter === 1 || vLine.cusomCounter === V_CELLS) { // первая/последняя строка
//                     if (i % 2 === 1) {   // нечётные ячейки в нечётных строках
//                         hCell.style.cssText += `background-color:${DARK_CELL}`; // делаем чёрной
//                     }
//                 } else { // не первая/последняя
//                     if (i % 2 === 1 && (i === 1 || i === H_CELLS)) {   // нечётные ячейки в нечётных строках, но только первая/последняя ячейка в строке
//                         hCell.style.cssText += `background-color:${DARK_CELL}`; // делаем чёрной
//                     }
//                 }
//             }
//             vLine.append(hCell);
//         }
//         board.append(vLine);
//     }
//
//     document.body.prepend(fragment);
//
// };


fillChessBoard();



