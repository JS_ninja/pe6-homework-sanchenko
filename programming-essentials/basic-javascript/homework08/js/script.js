const root = window.root;

const labelElem = document.createElement("label");
labelElem.textContent = "Price";

const inputElem = document.createElement("input");
inputElem.type = "number";
labelElem.append(inputElem);
inputElem.addEventListener("focus", () => {
    if (errorElem.style.visibility === "hidden"){
        labelElem.style.backgroundColor = "#3db489";
        root.style.backgroundColor = "#9775b0";
    }
    inputElem.style.color = "#000";
});
inputElem.addEventListener("blur", () => {
    root.style.backgroundColor = "white";
    if (inputElem.value < 0) {
        labelElem.style.backgroundColor = "#dc143c";
        errorElem.style.visibility = "visible";
    } else if (!!inputElem.value) {
        labelElem.style.backgroundColor = "white";
        span.textContent = `Текущая цена: ${inputElem.value}`;
        inputElem.style.color = "#3db489";
        span.style.visibility = "visible";
        errorElem.style.visibility = "hidden";
        span.append(closeButton);
    } else {
        labelElem.style.backgroundColor = "white";
    }
});
inputElem.addEventListener("keyup", () => {
    if (errorElem.style.visibility !== "hidden"){
        if (+inputElem.value >= 0 || !inputElem.value ){
            errorElem.style.visibility = "hidden";
            labelElem.style.backgroundColor = "#3db489";
            root.style.backgroundColor = "#9775b0";
        }
    }
});

const spanHolder = document.createElement("div");
spanHolder.style.cssText = "margin-bottom: 15px;";
root.prepend(spanHolder);

const span = document.createElement("span");
span.textContent = `Текущая цена: ${inputElem.value}   `;
spanHolder.append(span);

const closeButton = document.createElement("div");
closeButton.classList.add("closeButton");
//
closeButton.addEventListener("mousedown", (event) => {
    span.style.visibility = "hidden";
    inputElem.value = "";
    event.preventDefault();
});

const crossButtonFirstPart = document.createElement("::after");
closeButton.append(crossButtonFirstPart);

const crossButtonSecondPart = document.createElement("::after");
closeButton.append(crossButtonSecondPart);

const errorElem = document.createElement("div");
errorElem.classList.add("errorElem");
errorElem.textContent = "Please enter correct price";
labelElem.append(errorElem);

root.append(labelElem);



