const tabsTitles = [];
document.querySelectorAll(".tabs-title").forEach((el)=>tabsTitles.push(el));
const tabsContents = [];
document.querySelectorAll(".tabs-content > li").forEach((el)=>tabsContents.push(el));
for (let i=1; i < tabsContents.length; i++){
    tabsContents[i].style.display = "none";
}

tabsTitles.forEach((li) => {
    li.addEventListener("click", () => {
        let previous = document.querySelector(".tabs-title.active");
        previous.classList.remove("active");
        li.classList.add("active");

        tabsContents.forEach((el)=>el.style.display = "none");
        tabsContents[tabsTitles.indexOf(li)].style.display = "list-item";
    })
});

