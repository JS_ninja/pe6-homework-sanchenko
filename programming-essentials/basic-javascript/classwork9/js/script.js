// //TASK 1
//
// const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday',];
//
// for(let i = 0; i < days.length; i++){
//     console.log(days[i]);
// }
//
// for(const value of days){
//     console.log(value)
// }
//
// days.forEach((value)=>console.log(value));
//
// //===================================

// //TASK 2
//
// function organizer() {
//
//     const days = {
//         ua: ['Понеділок', 'Вівторок', 'Середа', 'Четвер',
//             'П’ятниця', 'Субота', 'Неділя',],
//         ru: ['Понедельник', 'Вторник', 'Среда',
//             'Четверг', 'Пятница', 'Суббота', 'Воскресенье',],
//         en: ['Monday', 'Tuesday',
//             'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday',],
//     };
//
//     let chosenLanguage = "";
//     do {
//         chosenLanguage = prompt("Choose your language (ua, ru, en)");
//     } while (!Object.keys(days).includes(chosenLanguage));
//
//     return days[chosenLanguage].join(", ");
//
// }
//
// console.log(organizer());
//
// //===================================

// //TASK 3

function mergeArrays(...props) {
    console.log(props);

    for (i = 0; i < props.length; i++) {
        if (!Array.isArray(props[i])) {
            return `Error in ${i + 1} element`
        }
    }

    let result = [];

    props.forEach((el) => {result.push(...el)});

    return result;
}

console.log(mergeArrays([1, 2, 3], [4, 5, 6], [7, 8, 9]));





























