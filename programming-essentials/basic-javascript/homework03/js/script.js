let previousA = "";

let a = prompt("Write first number", previousA);

if (a !== null) {
    previousA = a
} // Чтобы, когда prompt будет возвращать null, в окне ввода выводилось воследнее введённое значение, а не "null"

while (!isNumber(a)) {
    alert("Error! Try again.");
    a = prompt("Write first number", previousA);
    if (a !== null) {
        previousA = a
    }
}


let previousB = "";

let b = prompt("Write second number", previousB);

if (b !== null) {
    previousB = b
}

while (!isNumber(b)) {
    alert("Error! Try again.");
    b = prompt("Write second number", previousB);
    if (b !== null) {
        previousB = b
    }
}


let previousC = "";

let c = prompt("Write operation", previousC);

if (c !== null) {
    previousC = c
}

while (c !== "+" && c !== "-" && c !== "*" && c !== "/") {
    alert("Error! Try again.");
    c = prompt("Write operation", previousC);
    if (c !== null) {
        previousC = c
    }
}

// console.log("a = " + a);
// console.log("b = " + b);
// console.log("c = " + c);

function operation(a, b, c) {
    switch (c) {
        case "+":
            return a + b;
        case "-":
            return a - b;
        case "*":
            return a * b;
        case "/":
            return a / b;
        default:
            console.log("How did you do this?")
    }
}

console.log(operation(a, b, c));