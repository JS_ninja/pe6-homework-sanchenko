"use strict";

// const btn = document.querySelector("button");
// console.log(btn);

// btn.onclick = function () {
//     alert("Hi");
// };
// btn.onclick = function () {
//     alert("Hi all");
// };

// btn.addEventListener("click", function () {
//     alert("Hi all");
// });

// btn.addEventListener("click", () => {
//     console.log(this);
// });

// btn.addEventListener("click", btnClickHandler);

// function btnClickHandler(e) {
//     console.log(e.target === this);
//     console.log(e);

//     if (e.ctrlKey) {
//         btn.removeEventListener("click", btnClickHandler);
//     }
// }

// function onBtnClick(event) {
//     console.log(event.target === this);
// }

// TASK 1 TASK 2

// const root = document.querySelector("#root");
//
// const buttonCreator = function(){
//     const button = document.createElement('button');
//     button.textContent = "Войти";
//     button.addEventListener('click', ()=>{alert("Добро пожаловать!")});
//     const mouseoverHandler = function(){
//         alert("При клике по кнопке вы войдёте в систему");
//         button.removeEventListener('mouseover', mouseoverHandler)
//     };
//     button.addEventListener('mouseover', mouseoverHandler);
//     root.append(button);
// };
//
// buttonCreator();

// TASK 3 TASK 4

// const ROOT = document.querySelector("#root");
// const PHRASE = 'Добро пожаловать!';
//
// function getRandomColor() {
//     const r = Math.floor(Math.random() * 255);
//     const g = Math.floor(Math.random() * 255);
//     const b = Math.floor(Math.random() * 255);
//     return `rgb(${r}, ${g}, ${b})`;
// }
//
// function changeColors() {
//     let arr = header.innerText.split("");
//     for (let i = 0; i < arr.length; i++) {
//         arr[i] = `<span style="color: ${getRandomColor()}">${arr[i]}</span>`;
//     }
//     header.innerHTML = arr.join("");
// }
//
//
//
//
// const header = document.createElement("h1");
// header.innerText = PHRASE;
// const button = document.createElement("button");
// button.innerText = "Раскрасить";
//
// const fragment = document.createDocumentFragment();
// fragment.append(header);
// fragment.append(button);
//
// ROOT.append(fragment);
//
// // function x5() {
// //     let interval = window.setInterval(()=>{changeColors()}, 300);
// //     window.setTimeout(()=>{clearInterval(interval)}, 10000);
// // };
// // button.addEventListener("click", x5);
//
// button.addEventListener("click", changeColors);
//
// document.addEventListener("mousemove", changeColors);

// TASK 5

// const block1 = document.querySelector("#block-1");
// const block2 = document.querySelector("#block-2");
//
// function changeBlock2() {
//     if (block2.style.background === "green") {
//         block2.style.background = "white";
//     } else {
//         block2.style.background = "green";
//     }
// }
//
// function changeBlock1() {
//     if (block1.style.background === "red") {
//         block1.style.background = "white";
//     } else {
//         block1.style.background = "red";
//     }
// }
//
// block1.addEventListener("click", changeBlock2);
// block2.addEventListener("click", changeBlock1);

// TASK 6

const input = document.querySelector("#input");
const button = document.querySelector("#validate-btn");

const isValid = function(value) {
    return !!value && value.length > 4 && value.split(" ").join("") === value;
};

let timeout;

function validator(){
    if(timeout){
        clearTimeout(timeout);
    }
    if(isValid(button.value)){
        button.style.background = "green";
        button.textContent = "VALID";
        timeout = window.setTimeout(()=>{
            button.style.background = "white";
            button.textContent = "Validate";
        }, 2000);
    } else {
        button.style.background = "red";
        button.textContent = "INVALID";
        timeout = window.setTimeout(()=>{
            button.style.background = "white";
            button.textContent = "Validate";
        }, 2000);
    }
}

button.addEventListener("click", validator);



