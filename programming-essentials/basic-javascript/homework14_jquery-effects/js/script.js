// jQuery
let windowHeight = document.documentElement.clientHeight;

$(window).resize(function () {
    windowHeight = document.documentElement.clientHeight;
});

$(document).ready(function () {
    $(".navbar__list").on("click", "a", function (event) {
        event.preventDefault();
        let name = $(this).attr('href');
        console.log(`Now we're going to ${name}`);
        let top = $(`[name = ${name.slice(1)}]`).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });

    $("footer").on("click", ".go-to-top-button", function (event) {
        event.preventDefault();
        $('body,html').animate({scrollTop: 0}, 1500);
    });

    $(document).scroll(function (e) {
        if ($(window)[0].scrollY > windowHeight) {
            $(".go-to-top-button").removeClass("hide-me")
        } else {
            $(".go-to-top-button").addClass("hide-me")
        }
    });

    $(".most-popular__heading").on("click", ".toggler", function (e) {
        if ($(".toggler")[0].textContent !== "(TOGGLE IT >)") {
            $(".toggler")[0].textContent = "(TOGGLE IT >)";
        } else {
            $(".toggler")[0].textContent = "(TOGGLE IT <)";
            console.log("hello");
        }
        $(".most-popular > .container").slideToggle(1500);
    });
});

