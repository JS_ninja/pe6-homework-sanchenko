let previousA = "";
let a = prompt("Write a number", previousA);
if (a !== null) {
    previousA = a
} // Чтобы, когда prompt будет возвращать null, в окне ввода выводилось воследнее введённое значение, а не "null"
while (!isNumber(a) || parseInt(+a) !== +a) {
    alert("Error! Try again.");
    a = prompt("Write a number", previousA);
    if (a !== null) {
        previousA = a
    }
}

let multipleCheck = false;

if ( a >= 0){
    for (let i = 0; i <= a; i++) {
        if (i % 5 === 0) {
            console.log(i);
            multipleCheck = true;
                    }
    }
} else if ( a < 0 ){
    for (let i = 0; i >= a; i--) {
        if (i % 5 === -0) { // проверяем кратность для отрицательных чисел
            console.log(i);
            multipleCheck = true
        }
    }
}

if (multipleCheck !== true){ // вряд ли такое может случиться
    alert("Sorry, no numbers!")
}

let confirmed = confirm("Wanna see optional advanced task?");

if (confirmed === true) {
    let m = 0;
    let n = 0;
    let b = "";
    let c = "";
    let previousB = "";
    let previousC = "";

    b = prompt("Write a number", previousB);
    if (b !== null) {
        previousB = b
    }
    while (!isNumber(b) || parseInt(+b) !== +b) {
        alert("Error! Try again.");
        b = prompt("Write a number", previousB);
        if (b !== null) {
            previousB = b
        }
    }


    c = prompt("Write a number", previousC);
    if (c !== null) {
        previousC = c
    }
    while (!isNumber(c) || parseInt(+c) !== +c) {
        alert("Error! Try again.");
        c = prompt("Write a number", previousC);
        if (c !== null) {
            previousC = c
        }
    }

    b = +b;
    c = +c;

    if (b > c) {
        m = b;
        n = c;
    } else {
        m = c;
        n = b;
    }

    for (let i = n; i <= m; i++) {
        let j = 2;
        if(i === 2){
            console.log(i)
        }
        while (j < i) {
            if(i%j===0){
                break
            }
            if (j===i-1){
                console.log(i)
            }
            j++;
        }
    }

}
