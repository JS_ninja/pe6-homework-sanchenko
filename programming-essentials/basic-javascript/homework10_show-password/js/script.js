const firstInput = document.querySelector("body > form > label:nth-child(1)");
const secondInput = document.querySelector("body > form > label:nth-child(2)");

firstInput.children[2].style.display = "none";
secondInput.children[2].style.display = "none";

function handler() {
    if (this.parentElement.children[0].type === "password") {
        this.parentElement.children[0].type = "";
        this.parentElement.children[1].style.display = "none";
        this.parentElement.children[2].style.display = "block";
    } else {
        this.parentElement.children[0].type = "password";
        this.parentElement.children[2].style.display = "none";
        this.parentElement.children[1].style.display = "block";
    }
}

firstInput.children[1].addEventListener("click", handler);
firstInput.children[2].addEventListener("click", handler);
secondInput.children[1].addEventListener("click", handler);
secondInput.children[2].addEventListener("click", handler);

const button = document.querySelector("button");
const errorMessage = document.createElement("div");
errorMessage.textContent = "Нужно ввести одинаковые значения";
errorMessage.style.color = "red";
errorMessage.style.marginBottom = "15px";
errorMessage.style.visibility = "hidden";
button.before(errorMessage);

button.type = "";

button.addEventListener("click", () => {
    if (!firstInput.children[0].value && !secondInput.children[0].value) {
        errorMessage.style.visibility = "hidden";
        alert("Вы не ввели пароль");
    } else if (firstInput.children[0].value === secondInput.children[0].value) {
        errorMessage.style.visibility = "hidden";
        alert("You are welcome");
    } else {
        errorMessage.style.visibility = "";
    }
});