const root = window.root;
const beforeRoot = window.beforeRoot;
const fragment = document.createDocumentFragment();
const createList = function (arr, parent) {
    if (!parent) {
        parent = document.body;
    }

    let ol = document.createElement('ol');
    fragment.append(ol);

    let newArr = [].concat(arr);

    newArr.forEach((el) => {
        let li = document.createElement('li');
        if (Array.isArray(el)) {
            let ul = document.createElement('ul');
            el.forEach((elItem) => {
                let liItem = document.createElement('li');
                liItem.textContent = elItem;
                ul.append(liItem);
            });
            li.append(ul);
        } else {
            li.textContent = el;
        }
        ol.append(li);
    });

    let timeBlock = document.createElement('div');
    let timeText = document.createElement('span');
    timeText.textContent = "Time left: ";
    timeBlock.append(timeText);
    let timeLeft = document.createElement('span');
    timeLeft.textContent = "3";
    timeLeft.style.cssText = "font-size: 1.5em; color: red;";
    timeBlock.append(timeLeft);
    fragment.append(timeBlock);

    parent.append(fragment);

    let interval = window.setInterval(() => {
        if (+timeLeft.textContent > 0) {
            timeLeft.textContent -= 1;
        } else {
            timeLeft.textContent = "Removing";
            clearInterval(interval);
            let i = 10;
            let anotherInterval = window.setInterval(() => {
                i -= 1;
                ol.style.cssText = `opacity: ${i / 10}`;
                timeBlock.style.cssText = `opacity: ${i / 10}`;
                if (i === 0) {
                    clearInterval(anotherInterval);
                    ol.remove();
                    timeBlock.remove();
                }
            }, 100);
        }
    }, 1000);
};

createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], root);
createList(["1", "2", "3", "sea", "user", 23], beforeRoot);
createList(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]);

