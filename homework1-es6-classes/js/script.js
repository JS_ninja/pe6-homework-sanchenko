class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(name) {
        this._name = name;
    }

    get age() {
        return this._age;
    }

    set age(age) {
        this._age = age;
    }

    get salary() {
        return this._salary;
    }

    set salary(salary) {
        this._salary = salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }
    get salary() {
        return 3 * this._salary;
    }
}

let qwerty = new Programmer("qwerty", 11, 1000, ["HTML", "CSS"]);
console.log(qwerty);
let petro = new Programmer("petro", 24, 1100, ["English", "Chinese", "Spanish"]);
console.log(petro);
let vasyl = new Programmer("vasyl", 17, 3000, ["C++"]);
console.log(vasyl);