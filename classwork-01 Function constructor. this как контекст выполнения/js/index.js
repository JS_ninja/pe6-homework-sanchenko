"use strict";

// const user = {
//     name: "Uasya",
//     age: 33,
//     getName() {
//         return this.name;
//     },
//     getAge: function () {
//         return () => {
//             return this.age;
//         };
//     },
// };

// console.log(this);
// console.log(user.getName());
// console.log(user.getAge()());

// function summ(a, b, c) {
//     return a + b + c;
// }
// // console.log(summ(1, 2, 3));

// function summKarr(a) {
//     return function (b) {
//         return function (c) {
//             return a + b + c;
//         };
//     };
// }

// const res = summKarr(1);
// console.log(res(2)(3));

// function createUser() {
//     return {
//         name: "Uasya",
//         age: 33,
//         getName() {
//             return this.name;
//         },
//         getAge: function () {
//             return () => {
//                 return this.age;
//             };
//         },
//     };
// }
// console.log(createUser());
// console.log(new Number(12));

// function CreateUser(name, age) {
//     this._age = 3;
//     this.name = name;
//     this.age = age;
//     this.getName = function () {
//         return this.name;
//     };
//     this.getAge = function () {
//         return () => {
//             return this.age;
//         };
//     };
// }

// const user = new CreateUser("Uasya", 18);
// console.log(user);

// Object.defineProperty(user, "age", {
//     set: function (value) {
//         if (value <= 20) {
//             this._age = value;
//         }
//     },
//     get: function () {
//         return this._age + 1;
//     },
// });

// user.age = 22;
// console.log(user.age);
// console.log(user._age);

// const user2 = new CreateUser("Uasiliy", 18);
// console.log(user2);

// const user3 = user;
// user3.name = "Vovan";
// console.log(user3);
// console.log(user);


// TASK 1

// const questionText = "Девиз дома Баратеонов";
// const questionAnswer = "Нам ярость!";
//
// const root = document.querySelector("#root");
//
// const CreateQuestion = function () {
//     this.question = questionText;
//     this.answer = questionAnswer;
//
//     this.renderQuestion = function () {
//         const tagA = document.createElement("a");
//         tagA.textContent = this.question;
//         root.append(tagA);
//         let flag = false;
//         const tagP = document.createElement("p");
//         tagP.textContent = this.answer;
//         tagP.style.display = "none";
//         tagA.after(tagP);
//         tagA.addEventListener("click", () => {
//             if (flag) {
//                 flag = false;
//                 tagP.style.display = "none";
//             } else {
//                 flag = true;
//                 tagP.style.display = "block";
//             }
//         })
//     }
// };
//
// const question = new CreateQuestion(questionText, questionAnswer);
//
// question.renderQuestion();


// TASK 2

const render = function () {
    let code = document.createElement("div");
    code.innerHTML = "<h2 id=\"time\"></h2>\n" +
        "<button id=\"start-time\">Start</button>\n" +
        "<button id=\"stop-time\">Stop</button>\n" +
        "<button id=\"reset-time\">Reset</button>";
    root.append(code);
};
render();

function isNumber(x) {
    return !(!(x && !isNaN(+x) && typeof +x === "number"));
}


function Stopwatch(container) {
    this._time = 0;
    this.container = container;

    this.setTime = function (time) {
        if (isNumber(time)) {
            this._time = time;
        } else {
            return {status: "error", message: "argument must be positive integer"};
        }
    };
    this.getTime = function () {
        return this._time;
    };

    this.start = function () {
        this.interval = setInterval(() => {
            this.setTime(this._time + 1);
            this.container.innerText = this._time;
        }, 1000);
    };

    this.stop = function () {
        clearInterval(this.interval);
    };

    this.reset = function () {
        this._time = 0;
        this.container.textContent = this._time;
    }
}


const startBtn = document.getElementById('start-time');
const stopBtn = document.getElementById('stop-time');
const resetBtn = document.getElementById('reset-time');

const stopWatchContainer = document.getElementById('time');
const stopwatch = new Stopwatch(stopWatchContainer);


startBtn.addEventListener('click', stopwatch.start.bind(stopwatch));
stopBtn.addEventListener('click', stopwatch.stop.bind(stopwatch));
resetBtn.addEventListener('click', stopwatch.reset.bind(stopwatch));

